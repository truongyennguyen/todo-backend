import {
    Injectable,
    ExecutionContext,
    CanActivate,
    UnauthorizedException
  } from '@nestjs/common'
  // import { AuthGuard } from '@nestjs/passport'
  
  import { UserService } from '../../modules/user/user.service'
  
  @Injectable()
  export class AuthGuard implements CanActivate {
    constructor(private readonly userService: UserService) { }
  
    async canActivate(
      context: ExecutionContext,
    ) {
      try {
          // get token from http request
        const { token } = context.switchToHttp().getRequest().headers

        if (!token) {
          throw new UnauthorizedException('Unauthorized')
        }
  
        const userDecode = await this.userService.decodeToken(token)

        // không có context 
        return true

      } catch (err) {
        throw err
        // return false
      }
    }
  }
  