import {
    Injectable,
    ExecutionContext,
    CanActivate,
    UnauthorizedException,
		ForbiddenException
  } from '@nestjs/common'
  // import { AuthGuard } from '@nestjs/passport'
  
  import { Observable } from 'rxjs'
  import { UserService } from '../../modules/user/user.service'
	import { Reflector } from '@nestjs/core'
  
  @Injectable()
  export class AuthorGuard implements CanActivate {
    constructor(private readonly userService: UserService, private readonly reflector: Reflector) { }
  
    async canActivate(
      context: ExecutionContext,
    ) {
      try {
				// get role from decorator
				const role = this.reflector.get<string>('role', context.getHandler());

				//get token from http request
				const { token } = context.switchToHttp().getRequest().headers

        if (!token) {
          throw new UnauthorizedException('Unauthorized')
				}
	
				// decode token
				const userDecode = this.userService.decodeToken(token)			
				
				if(role !== userDecode.role) {
					throw new ForbiddenException('Permission Denied')
				}

				return true

      } catch (err) {
        throw err
        // return false
      }
    }
  }
  