import { Controller, Get, UseGuards, Body, Post, Delete, Param, Headers } from '@nestjs/common';
import { TaskService } from './task.service';
import { AuthGuard } from 'src/common/guard/auth.guard';

@Controller('task')
export class TaskController {
	constructor(
		private readonly taskService: TaskService
	){}

	@Get()
	@UseGuards(AuthGuard)
	async tasks(@Headers('token') token: string): Promise<any> {
		return await this.taskService.getAllByToken(token)
	}

	@Post('/create')
	@UseGuards(AuthGuard)
	async create (@Body() body, @Headers('token') token: string): Promise<any>{
		return await this.taskService.create({ ...body }, token)
	}

	@Post('/update')
	@UseGuards(AuthGuard)
	async update (@Body() body, @Headers('token') token: string): Promise<any>{
		return await this.taskService.update({ ...body }, token)
	}

	@Delete('delete/:id')
	@UseGuards(AuthGuard)
	async delete (@Body() body): Promise<any>{
		return await this.taskService.delete({ ...body })
	}

}
