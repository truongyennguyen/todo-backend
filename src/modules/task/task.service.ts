import { Injectable, NotFoundException } from '@nestjs/common';
import * as fs from 'fs'
import { join } from 'path'
import { UserService } from '../user/user.service';

@Injectable()
export class TaskService {
	constructor(
		private readonly userService: UserService
	){}
	read() {
    const data = fs.readFileSync(join(__dirname, '..', '../../src/data/tasks.json'), 'utf8')
    return JSON.parse(data)
  }

  write (data) {
    const jsonData = JSON.stringify(data)
    fs.writeFileSync(join(__dirname, '..', '../../src/data/tasks.json'), jsonData, 'utf8')
	}
	
  getAllByToken(token: string) {
		const decodeUser = this.userService.decodeToken(token)
		const tasks = this.read()
		return tasks.filter(task => task.userId === decodeUser._id)
	}

	getAllByUserId (userId: string) {
		const tasks = this.read()
		return tasks.filter(task => task.userId === userId)
	}

	//priority = 1 là cực kì quan trọng, = 2 là quan trọng, = 3 là bình thường
	create({ name, description, dateBegin, dateEnd, priority, isComplete = false }, token: string) {
		const decodeUser = this.userService.decodeToken(token)
		const tasks = this.read()
		const newTask = { _id: `${tasks.length + 1}`, name, description, dateBegin, dateEnd, priority, isComplete, userId: decodeUser._id }
		this.write([...tasks, newTask])
		return newTask
	}

	update({ _id, name, description, dateBegin, dateEnd, priority, isComplete }, token: string) {
		const decodeUser = this.userService.decodeToken(token)
		const tasks = this.read()
		let taskUpdate = tasks.find(task => task._id === _id)
		
		if(!taskUpdate) {
			return new NotFoundException('Not found task')
		} else {
			let newTasks = [...tasks]
			const newTask = { _id: `${_id}`, name, description, dateBegin, dateEnd, priority, isComplete, userId : decodeUser._id}
			newTasks[_id - 1] = newTask
			this.write(newTasks)
			return newTask
		}
	}

	delete(_id) {
		const tasks = this.read()
		const newTasks = [...tasks].splice(_id - 1, 1)
		this.write(newTasks)
	}

}
