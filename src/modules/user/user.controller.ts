import { Controller, Get, Param, Post, UnauthorizedException, UseGuards, Body } from '@nestjs/common'
import { UserService } from './user.service'
import { AuthGuard } from 'src/common/guard/auth.guard'
import { AuthorGuard } from 'src/common/guard/author.guard'
import { Role } from 'src/common/decorator/role.decorator'

@Controller('user')
export class UserController {
	constructor(
    private readonly userService: UserService
	) {}
	
	// get all user
	@Get()
	@Role('admin')
	@UseGuards(AuthGuard)
	@UseGuards(AuthorGuard)
	async users (): Promise<any> {
		return await this.userService.getAll()
	}

	// login
	@Post('/login')
	async login (@Body() body): Promise<string | UnauthorizedException> {
		return await this.userService.login(body.username, body.password)
	}

	// registry
	@Post('/registry')
	async registry (@Body() body): Promise<any> {
		return await this.userService.create({...body})
	}

	// create new user
	@Role('admin')
	@UseGuards(AuthGuard)
	@UseGuards(AuthorGuard)
	@Post('/create')
	async create (@Body() body): Promise<any> {
		return await this.userService.create({...body})
	}
}

