import { Injectable, UnauthorizedException, NotFoundException, ConflictException } from '@nestjs/common';
import * as fs from 'fs'
import { join } from 'path'
import * as jwt from 'jsonwebtoken'

@Injectable()
export class UserService {
  // đọc data từ file
  read() {
    const data = fs.readFileSync(join(__dirname, '..', '../../src/data/users.json'), 'utf8')
    return JSON.parse(data)
  }

  // ghi data vào file
  write (data) {
    const jsonData = JSON.stringify(data)
    fs.writeFileSync(join(__dirname, '..', '../../src/data/users.json'), jsonData, 'utf8')
  }

  // decode token
  decodeToken (token: string) {
    console.log(token)
    const userDecode = jwt.decode(token, { complete: true })
    const users = this.read()
    const user = users.find(item => item.username === userDecode.payload.username)
    if(user) {
      return { _id: user._id, name: user.name, username: user.username, role: user.role }
    }
    return {}
  }

  // lấy toàn bộ thông tin user
  getAll() {
    return this.read()
  }


  // đăng nhập
  login(username: string, password: string) {
    const users = this.read()
    const user = users.find(item => (item.username === username && item.password === password))

    if(!user) {
      return new UnauthorizedException('Username or password incorrect!!!')
    }

    const token = jwt.sign({
      _id: user._id,
      name: user.name,
      username: user.username,
      role: user.role
    },
    'todo',
    {
      expiresIn: '30d'
    })
    return token
  }

  getByUsername (username: string) {
    const users = this.read()
    const user = users.find(item => item.username === username)
    if(user){
      return user
    }
    return new NotFoundException('Not found')
  }

  // tạo mới user
  create ({ name, username, password, role = 'member' }) {
    const users = this.read()
    const user = users.find(item => item.username === username)
    if(user) {
      return new ConflictException('Conflict username')
    }

    const newId = `${users.length + 1}`
    const newUser = { _id: newId, name, username, password, role }
    const newUsers = [...users, newUser]

    this.write(newUsers)
    return { _id: newId, name, username, role }
  }
}
